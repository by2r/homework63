<?php

namespace App\Http\Controllers;

use App\Models\FollowerUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function show() {
        $user = Auth::user();
        if(!$user) {
            return redirect()->route('home')->with('error', 'You have to log in first!');
        }
        return view('profile', compact('user'));
    }
    public function index() {
        $users = User::all();
        return view('find_friends', compact('users'));
    }

    public function search() {
        $users = User::all();
        return view('find_friends', compact('users'));
    }

    public function follow(Request $request, User $user) {
        $me = $request->user();
        $user = User::find($request->input('user'));
        $following = new FollowerUser(['user_id' => $user->id, 'follower_id' => $me->id]);
        if (FollowerUser::all()->where('user_id', $user->id)->where('follower_id', $me->id)->count() > 0) {
            return redirect()->route('find_friends')->with('error', 'You already follow ' . $user->name);
        }
        $following->save();
        return redirect()->route('find_friends')->with('success', 'You now follow' . $user->name);
    }
}
