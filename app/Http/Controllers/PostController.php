<?php

namespace App\Http\Controllers;


use App\Models\FollowerUser;
use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\Image as Img;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $followings = FollowerUser::where('follower_id', Auth::id())->get('user_id')->toArray();
        $followings_ids = [];
        foreach($followings as $value) {
            $followings_ids[] = $value['user_id'];
        }
        $posts = Post::all()->whereIn('user_id', $followings_ids);
        return view('home', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'image' => 'required',
            'description' => 'required|min:1|max:255',
            'user_id' => 'required'
        ]);
        $post = new Post($validated);
        $post->save();
        $image = new Img();
        $this->getImage($validated);
        $image->image = $validated['image'];
        $image->post_id = $post->id;
        $image->save();
        return redirect()->route('profile')->with('success', "The Post is successfully added!");
    }

    private function getImage(&$validated) {
        $file = data_get($validated, 'image');
        if (!is_null($file)) {
            $f = Image::make($file)->fit(300)->encode('jpg');
            $image_name = md5($file);
            Storage::disk('public')->put('pictures/'.$image_name, $f->__toString());
            $validated['image'] = 'pictures/' . $image_name;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        $validated = $request->validate([
            'image' => 'required',
            'description' => 'required|min:1|max:255',
            'user_id' => 'required'
        ]);
        $post->update($validated);
        $image = new Img();
        $this->getImage($validated);
        $image->image = $validated['image'];
        $image->post_id = $post->id;
        $image->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('profile')->with('success', 'The post is successfully deleted!');
    }

    public function like(Request $request) {
        $post_id = $request->input('post_id');
        $like = Like::all()->where('user_id', Auth::id())->where('post_id', $post_id);
        $like_exists = $like->count() > 0;
        if ($like_exists) {
            $like->first()->delete();
            return redirect()->route('home')->with('success', 'You unlike a Post!');
        }
        $like = new Like();
        $like->user_id = Auth::id();
        $like->post_id = $post_id;
        $like->save();
        return redirect()->route('home')->with('success', 'You like a Post!');
    }
}
