<?php
namespace App\Orchid\Screens\Comments;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
class EditScreen extends Screen
{
    private Comment $comment;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Comment $comment): iterable
    {
        $this->comment = $comment;
        return [
            'comment' => $comment
        ];
    }
    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        if (!$this->comment->exists) {
            return 'Создание статьи';
        }
        return 'Редактирование статьи: ' . $this->comment->content;
    }
    public function description(): ?string
    {
        return 'Создание/редактирование комментария';
    }
    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Save')
                ->icon('bs.check-circle')
                ->canSee(!$this->comment->exists)
                ->method('store'),
            Button::make('Update')
                ->icon('bs.note')
                ->method('update')
                ->canSee($this->comment->exists),
            Button::make('Remove')
                ->icon('bs.trash')
                ->method('remove')
                ->canSee($this->comment->exists),
        ];
    }
    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('comment.post_id')->title('Post')
                    ->fromModel(Post::class, 'id', 'id'),
                Quill::make('comment.content')
                    ->title('Main text'),
                Quill::make('comment.approved')->title('Approved'),

            ])
        ];
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'comment.user_id' => ['required', 'exists:users,id'],
            'comment.content' => ['required', 'min:5', 'max:2048']
        ], [
            'comment.content.required' => 'Comment content is required'
        ]);
        $this->comment = new Comment();
        $this->comment->fill($request->get('comment'))->save();

        Alert::info('You have successfully created an comment.');

        return redirect()->route('platform.comments.list');
    }

    public function update(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();

        Alert::info('You have successfully created a comment.');

        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('You have successfully deleted the comment.')
            : Alert::warning('An error has occurred')
        ;

        return redirect()->route('platform.comments.list');
    }
}
