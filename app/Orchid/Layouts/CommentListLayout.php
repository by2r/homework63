<?php
namespace App\Orchid\Layouts;
use App\Models\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')->sort()->render(function (Comment $comment) {
                return Link::make($comment->id)
                    ->route('platform.comments.edit', $comment);
            }),
            TD::make('created_at', 'Created')->sort()
                ->render(function (Comment $comment) {
                    return $comment->created_at->diffForHumans();
                }),
            TD::make('updated_at', 'Last edit')->sort()->render(function (Comment $comment) {
                return '<strong style="color: red">' .
                    $comment->updated_at->diffForHumans() .
                    '</strong>';
            }),
            TD::make('approved', 'Approved')->sort()
        ];
    }
}
