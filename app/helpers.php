<?php

function get_likes_count($post)
{
    return App\Models\Like::where('post_id', $post->id)->select('id')->get()->count();
}

function get_followers($user) {
    $followers = App\Models\FollowerUser::all()->where('user_id', $user->id);
    $result = [];
    foreach ($followers as $follower) {
        $result[] = (\App\Models\User::all()->where('id', $follower->follower_id));
    }
    return $result;
}

function get_followings($user) {
    $followings = App\Models\FollowerUser::all()->where('follower_id', $user->id);
    $result = [];
    foreach ($followings as $following) {
        $result[] = (\App\Models\User::all()->where('id', $following->user_id));
    }
    return $result;
}
