<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Comment extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    protected $fillable = ['content', 'user_id', 'post_id', 'approved'];
    protected $allowedFilters = [
        'content' => \Orchid\Filters\Types\Like::class
    ];

    protected $allowedSorts = [
        'id', 'content', 'created_at', 'updated_at'
    ];
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function post() {
        return $this->belongsTo(Post::class);
    }
}
