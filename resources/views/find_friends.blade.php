@extends('layouts.app')
@section('content')
    @foreach($users as $user)
        <div class="mb-3">
            <strong>{{$user->name}}</strong> |
            <form action="{{action([\App\Http\Controllers\ProfileController::class, 'follow'], compact('user'))}}" class="d-inline" method="post">
                @csrf
                @method('POST')
                <input type="hidden" name="user" value="{{$user['id']}}">
                <button type="submit" class="btn btn-outline-primary">Follow</button>
            </form>
        </div>
    @endforeach
@endsection
