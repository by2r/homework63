@extends('layouts.app')
@section('content')
    @foreach($posts as $post)
        <div>
            <div id="carouselExample" class="carousel slide w-50 mt-5">
                <div class="carousel-inner">
                    @for($i=0; $i < count($post->images); $i++)
                        <div class="carousel-item @if($i==0)active @endif">
                            <img class="d-block w-100" src="{{asset('storage/' . $post->images[$i]->image)}}" alt="Post image">
                        </div>
                    @endfor
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <div class="d-inline">
                <form action="{{action([\App\Http\Controllers\PostController::class, 'like'])}}" class="mt-2">
                    <input type="hidden" value="{{$post->id}}" name="post_id">
                    <button class="btn btn-outline-danger">Like</button>
                </form>
            </div>
            <div class="fst-italic mt-1">
                Likes: {{get_likes_count($post)}}
            </div>
            <div class="w-50 fw-bold my-3">
                {{$post->description}}
            </div>
            <div class="w-50">
                @foreach($post->comments as $comment)
                    @if($comment->approved)
                        <div class="border border-1 my-2 p-3">
                            @can('delete-comment', $comment)
                                <form action="{{action([\App\Http\Controllers\CommentController::class, 'destroy'], compact('comment'))}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="float-end" type="submit">X</button>
                                </form>
                            @endcan
                            <p class="fst-italic mb-0">{{$comment->user->name}} commented {{$comment->created_at->diffForHumans()}}:</p>

                            <p class="mb-0">{{$comment->content}}</p>
                        </div>
                    @endif
                @endforeach
                <form action="{{action([\App\Http\Controllers\CommentController::class, 'store'])}}" method="post">
                    @csrf
                    @method('POST')
                    <div class="mb-3">
                        <label for="content" class="form-label">Leave a comment:</label>
                        <textarea name="content" class="form-control mb-3" id="content" cols="30" rows="10"></textarea>
                    </div>
                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    @endforeach
@endsection
