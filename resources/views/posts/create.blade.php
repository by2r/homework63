@extends('layouts.app')
@section('content')
    <form action="{{action([\App\Http\Controllers\PostController::class, 'store'])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('POST')
        <label for="image" class="form-label">Upload Image</label>
        <input type="file" class="form-control mb-3" name="image" id="image">
        <div class="mb-3">
            <label for="description" class="form-label">Post Description:</label>
            <textarea name="description" class="form-control mb-3" id="description" cols="30" rows="10"></textarea>
        </div>
        <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
