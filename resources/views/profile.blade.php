@extends('layouts.app')
@section('content')
    <h2>{{$user->name}}</h2>
    <div class="row border border-1 rounded p-3">
        <div class="col-6">
            <h4>Your Followers: </h4>
            @foreach(get_followers(\Illuminate\Support\Facades\Auth::user()) as $follower)
                <div>{{$follower->first()->name}}</div>
            @endforeach
        </div>
        <div class="col-6">
            <h4>Your Followings: </h4>
            @foreach(get_followings(\Illuminate\Support\Facades\Auth::user()) as $following)
                <div>{{$following->first()->name}}</div>
            @endforeach
        </div>
    </div>
    <div class="mx-5">
        <h4 class="mt-5">Your posts:</h4>
        <div class="row">
            @if(!empty($user->posts))
                @foreach($user->posts as $post)
                    <div class="col-4 border border-1 p-5">
                        <img src="{{asset('storage/' . $post->images->first()->image)}}" alt="Post description">
                        <p class="fst-italic my-2">Likes: {{get_likes_count($post)}}</p>
                        <p class="m-0 mt-4">{{$post->description}}</p>
                        <form action="{{action([\App\Http\Controllers\PostController::class, 'destroy'], compact('post'))}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
