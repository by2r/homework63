<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Comment;
use App\Models\FollowerUser;
use App\Models\Image;
use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
         ]);

        User::factory()->count(10)->create();
        Post::factory()->count(50)->create();
        Comment::factory()->count(50*3)->create();
        Image::factory()->count(50)->create();
        Like::factory()->count(10*50)->create();
        for ($i = 1; $i <= 10; $i++){
                DB::table('follower_users')->insert([
                    'user_id' => $i,
                    'follower_id' => 1
                ]);
        }
    }
}
